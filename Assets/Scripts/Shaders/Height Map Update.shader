﻿Shader "BJ/Heght Map"
{
    Properties
    {
        _Radius ("Radius", float) = 0.1
    }
    SubShader
    {
        Pass
        {
            CGPROGRAM
            #include "UnityCustomRenderTexture.cginc"
            #pragma vertex CustomRenderTextureVertexShader
            #pragma fragment frag
            #pragma target 3.0
            
            float _Radius;
            float4 _DrawPosition;
            
            fixed4 frag (v2f_customrendertexture IN) : COLOR
            {
                fixed4 previousColor = tex2D(_SelfTexture2D, IN.localTexcoord.xy);
                fixed4 color = smoothstep(0, .2, distance(IN.localTexcoord.xy, _DrawPosition));
                
                return distance(IN.localTexcoord.xy, _DrawPosition) < _Radius ? float4(0,0,0,1) : previousColor;
            }
            ENDCG
        }
    }
}