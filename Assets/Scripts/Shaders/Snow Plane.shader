Shader "BJ/Snow Plane"
{
    Properties
    {
        _Color ("Top Color", Color) = (1,1,1,1)
        _BottomColor ("Bottom Color", Color) = (0.48, 1, 0.97, 1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _HeightMap ("Albedo (RGB)", 2D) = "white" {}
        _HeightAmount ("Height Amount", float) = 0.1
        _TessellationAmount ("Tessellation Amount", float) = 4
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
    }
    
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        #pragma surface surf Standard fullforwardshadows tessellate:tess
        #pragma vertex vert
        #pragma target 3.0

        sampler2D _MainTex;
        sampler2D _HeightMap;

        struct Input
        {
            float2 uv_MainTex;
        };

        half _Glossiness;
        half _Metallic;
        float _HeightAmount;
        float _TessellationAmount;
        
        fixed4 _Color;
        fixed4 _BottomColor;

        UNITY_INSTANCING_BUFFER_START(Props)
        UNITY_INSTANCING_BUFFER_END(Props)
        
        float tess()
        {
            return _TessellationAmount;
        }

        void vert(inout appdata_full v)
        {
            float offset = tex2Dlod(_HeightMap, float4(v.texcoord.xy, 0, 0)).r * _HeightAmount;
            v.vertex.y += offset;
        }

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            float height = tex2D(_HeightMap, IN.uv_MainTex).r;
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * lerp(_BottomColor, _Color, height);
            o.Albedo = c.rgb;
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Alpha = c.a;
        }
        
        ENDCG
    }
    
    FallBack "Diffuse"
}