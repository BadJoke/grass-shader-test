using UnityEngine;

public class SnowBrush : MonoBehaviour
{
    [SerializeField] private CustomRenderTexture _snowHeightMap;
    [SerializeField] private Material _snowHeightMaterial;

    private Camera _camera;
    
    private static readonly int DrawPosition = Shader.PropertyToID("_DrawPosition");

    private void Start()
    {
        _snowHeightMap.Initialize();
        _snowHeightMaterial.SetVector(DrawPosition, new Vector4(-1, -1, 0, 0));
        _camera = Camera.main;
    }

    private void  Update()
    {
        if (Input.GetMouseButton(0))
        {
            Ray ray = _camera.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out RaycastHit hit))
            {
                Vector2 textureCoordinate = hit.textureCoord;
                
                _snowHeightMaterial.SetVector(DrawPosition, textureCoordinate);
            }
        }   
    }
}
